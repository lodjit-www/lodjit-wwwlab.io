let didInit = false;
let logo, title, download;
let logoTop, titleTop, downloadTop;

function computeLogoTop()
{
  logoTop = document.getElementById("logo").getBoundingClientRect().top;
  logoTop += document.documentElement.scrollTop;
}

function computeTitleTop()
{
  titleTop = document.getElementById("title").getBoundingClientRect().top;
  titleTop += document.documentElement.scrollTop;
}

function computeDownloadTop()
{
  downloadTop = document.getElementById("download").getBoundingClientRect().top;
  downloadTop += document.documentElement.scrollTop;
}

function onWindowLoad()
{
  if (didInit) return;

  logo = document.getElementById("logo");
  title = document.getElementById("title");
  download = document.getElementById("download");

  computeLogoTop();
  computeTitleTop();
  computeDownloadTop();

  didInit = true;
}

function onWindowScrollOrResize()
{
  onWindowLoad();

  let scrollTop = document.documentElement.scrollTop;
  let elements = [
    [logo, logoTop, computeLogoTop],
    [title, titleTop, computeTitleTop],
    [download, downloadTop, computeDownloadTop]
  ]

  console.log("=============")
  for (let [element, elementTop, computeElementTop] of elements)
  {
    console.log(scrollTop, elementTop)
    if (scrollTop >= elementTop)
    {
      console.log("TRUE")
      element.classList.add("header");
    }
    else
    {
      console.log("FALSE")
      element.classList.remove("header");
      computeElementTop();
    }
  }
}

window.addEventListener("load", onWindowLoad);
window.addEventListener("scroll", onWindowScrollOrResize);
window.addEventListener("resize", onWindowScrollOrResize);
